# Linux Survey Form

## About

This is a survey form that could be used to collect data concerning
the popularity of certain Linux distributions and how they are used.
It is a prototype written solely in HTML and CSS so there is no submit
fuctionality.

[Click here](https://emerac.gitlab.io/linux-survey-form) to
view the page!

This page generally follows the requirements as laid out in
freeCodeCamp's Responsive Web Design Course, Project 2. However,
I chose to not follow the requirements exactly so that I could
excercise a little more creativity.

## License

This project is licensed under the GNU General Public License.
For the full license text, view the [LICENSE](LICENSE) file.
